import 'reseter.css';
import './styles/index.scss';
import './i18n';

import { Filter } from '@/features/Filter';
import { Sort } from '@/features/Sort';
import { SwitchLanguage } from '@/features/SwitchLanguage';
import { TicketsFeed } from '@/features/TicketsFeed';
import { ToUpOfPage } from '@/features/ToUpOfPage';
import { Layout, Logo } from '@/shared/ui';

export const App = () => {
  return (
    <Layout>
      <Layout.Header>
        <SwitchLanguage />
      </Layout.Header>
      <Logo src='assets/images/logo.png' />
      <Layout.Main>
        <Filter />
        <Layout.Container>
          <Sort />
          <TicketsFeed />
        </Layout.Container>
      </Layout.Main>
      <ToUpOfPage />
    </Layout>
  );
};

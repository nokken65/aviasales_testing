import { createEffect, createStore, forward } from 'effector';

import { api, SearchId, Ticket } from '@/shared/api';

const getSearchIdFx = createEffect<void, SearchId>(api.getSearchId);
const getTicketsFx = createEffect<SearchId, Array<Ticket>>(api.getTickets);

const $searchId = createStore<SearchId>({} as SearchId).on(
  getSearchIdFx.doneData,
  (_, payload) => payload
);

const $tickets = createStore<Array<Ticket>>([]).on(
  getTicketsFx.doneData,
  (_, payload) => payload
);

const $ticketsIsEmpty = $tickets.map((state) => state.length === 0);

forward({
  from: $searchId,
  to: getTicketsFx,
});

export const selectors = {
  $searchId,
  $tickets,
  $ticketsIsEmpty,
};

export const effects = {
  getSearchIdFx,
  getTicketsFx,
};

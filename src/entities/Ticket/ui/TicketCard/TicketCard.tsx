import { useTranslation } from 'react-i18next';

import { Card } from '@/shared/ui';
import { formatPrice } from '@/shared/utils';

import styles from './TicketCard.module.scss';
import { TicketSegment } from './TicketSegment';

type TicketCardProps = {
  ticket: import('@/shared/api').Ticket;
};

export const TicketCard = ({ ticket }: TicketCardProps) => {
  const { t } = useTranslation();
  return (
    <Card className={styles.ticketCard}>
      <div className={styles.ticketWrapper}>
        <p className={styles.price}>{`${formatPrice(ticket.price)} ${t(
          'ticket.price'
        )}`}</p>
        <img
          className={styles.aviaImg}
          src={`https://pics.avs.io/99/36/${ticket.carrier}.png`}
          loading='lazy'
          width={110}
          height={36}
          alt={ticket.carrier}
        />
        {ticket.segments.map((segment, i) => (
          <TicketSegment key={i} segment={segment} />
        ))}
      </div>
    </Card>
  );
};

import { useTranslation } from 'react-i18next';

import { formatFlyDurartion, formatFlyTime } from '@/shared/utils';

import styles from './TicketSegment.module.scss';
type TicketSegmentProps = {
  segment: import('@/shared/api').TicketSegment;
};

export const TicketSegment = ({ segment }: TicketSegmentProps) => {
  const { t } = useTranslation();
  return (
    <>
      <div className={styles.ticketSegment}>
        <h4 className={styles.segmentHeading}>
          {segment.origin} - {segment.destination}
        </h4>
        <p className={styles.segmentContent}>
          {formatFlyTime(new Date(segment.date).valueOf())} -{' '}
          {formatFlyTime(
            new Date(segment.date).valueOf() + segment.duration * 60000
          )}
        </p>
      </div>
      <div className={styles.ticketSegment}>
        <h4 className={styles.segmentHeading}>{t('ticket.onTheWay')}</h4>
        <p className={styles.segmentContent}>
          {formatFlyDurartion(segment.duration)}
        </p>
      </div>
      <div className={styles.ticketSegment}>
        <h4 className={styles.segmentHeading}>
          {t(
            `filter.${['direct', 'one', 'two', 'three'][segment.stops.length]}`
          )}
        </h4>
        {segment.stops.length ? (
          <p className={styles.segmentContent}>{segment.stops.join(', ')}</p>
        ) : null}
      </div>
    </>
  );
};

import { createEvent, createStore, sample } from 'effector';

import { ticketModel } from '@/entities/Ticket';
import { Ticket } from '@/shared/api';
import { TRANSFER_OPTIONS } from '@/shared/constants';
import { filterTickets } from '@/shared/utils';

const addActive = createEvent<TRANSFER_OPTIONS>();
const removeActive = createEvent<TRANSFER_OPTIONS>();
const $active = createStore<Array<TRANSFER_OPTIONS>>([-1])
  .on(addActive, (state, payload) => {
    if (payload === TRANSFER_OPTIONS.All) {
      return [TRANSFER_OPTIONS.All];
    } else {
      if (
        state.length === Object.keys(TRANSFER_OPTIONS).length / 2 - 2 &&
        !state.includes(payload)
      ) {
        return [TRANSFER_OPTIONS.All];
      } else {
        return [...state, payload].filter(
          (item) => item !== TRANSFER_OPTIONS.All
        );
      }
    }
  })
  .on(removeActive, (state, payload) => {
    if (
      state.includes(TRANSFER_OPTIONS.All) &&
      payload === TRANSFER_OPTIONS.All
    ) {
      return state;
    }
    return state.filter((item) => item !== payload);
  });

const $filteredTickets = createStore<Array<Ticket>>([]);

const $filteredTicketsIsEmpty = $filteredTickets.map(
  (tickets) => tickets.length === 0
);

sample({
  clock: [addActive, removeActive, ticketModel.effects.getTicketsFx.doneData],
  source: { tickets: ticketModel.selectors.$tickets, filter: $active },
  target: $filteredTickets,
  fn: filterTickets,
});

export const selectors = {
  $active,
  $filteredTickets,
  $filteredTicketsIsEmpty,
};

export const events = {
  addActive,
  removeActive,
};

import { reflect } from '@effector/reflect';
import { useTranslation } from 'react-i18next';

import { TRANSFER_OPTIONS } from '@/shared/constants';
import { Card, Checklist, Heading } from '@/shared/ui';

import { filterModel } from '..';

type FilterProps = {
  options: Array<string>;
  active: Array<TRANSFER_OPTIONS>;
  addActive: (id: TRANSFER_OPTIONS) => void;
  removeActive: (id: TRANSFER_OPTIONS) => void;
};

const FilterView = ({
  options,
  active,
  addActive,
  removeActive,
}: FilterProps) => {
  const { t } = useTranslation();
  return (
    <Card>
      <Heading>{t('filter.title')}</Heading>
      <Checklist>
        {options.map((item, id) => (
          <Checklist.Item
            label={t(`filter.${item}`)}
            key={id}
            active={active.includes(id - 1)}
            onActive={() =>
              active.includes(id - 1) ? removeActive(id - 1) : addActive(id - 1)
            }
          />
        ))}
      </Checklist>
    </Card>
  );
};

export const Filter = reflect({
  view: FilterView,
  bind: {
    options: ['all', 'direct', 'one', 'two', 'three'],
    active: filterModel.selectors.$active,
    addActive: filterModel.events.addActive,
    removeActive: filterModel.events.removeActive,
  },
});

import { combine, createEvent, createStore, sample } from 'effector';

import { filterModel } from '@/features/Filter';
import { sortModel } from '@/features/Sort';
import { Ticket } from '@/shared/api';
import { getTicketsPage, pagesRange } from '@/shared/utils';

const setPage = createEvent<number>();

const $pagesCount = combine(
  filterModel.selectors.$filteredTickets,
  (tickets) => {
    return Math.ceil(tickets.length / 10);
  }
);

const $currentPage = createStore<number>(1)
  .on(setPage, (state, payload) => {
    if (payload < state) {
      return payload < 1 ? 1 : payload;
    }
    if (payload > state) {
      return payload > $pagesCount.getState()
        ? $pagesCount.getState()
        : payload;
    }
  })
  .on(sortModel.events.changeActive, () => 1)
  .on(filterModel.events.addActive, () => 1)
  .on(filterModel.events.removeActive, () => 1);

const $pagesRange = createStore<Array<number>>([]);

const $currentTickets = createStore<Ticket[]>([]);

sample({
  clock: [
    filterModel.selectors.$filteredTickets,
    sortModel.selectors.$active,
    setPage,
  ],
  source: { page: $currentPage, pagesCount: $pagesCount },
  target: $pagesRange,
  fn: pagesRange,
});

sample({
  clock: [
    filterModel.selectors.$filteredTickets,
    sortModel.selectors.$active,
    setPage,
  ],
  source: {
    tickets: filterModel.selectors.$filteredTickets,
    page: $currentPage,
  },
  target: $currentTickets,
  fn: ({ tickets, page }) => getTicketsPage({ tickets, page, pageSize: 10 }),
});

export const selectors = {
  $currentPage,
  $currentTickets,
  $pagesCount,
  $pagesRange,
};

export const events = {
  setPage,
};

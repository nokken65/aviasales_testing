import { reflect } from '@effector/reflect';
import clsx from 'clsx';

import { Button } from '@/shared/ui';

import { paginationModel } from '..';
import styles from './Pagination.module.scss';

type PaginationProps = {
  pagesCount: number;
  currentPage: number;
  pagesRange: Array<number>;
  setPage: (n: number) => void;
};

const PaginationView = ({
  pagesCount,
  currentPage,
  pagesRange,
  setPage,
}: PaginationProps) => {
  return (
    <div className={styles.pagination}>
      <Button
        onClick={() => setPage(currentPage - 1)}
        disabled={currentPage === 1}
        className={styles.arrow}
      >
        {'<'}
      </Button>
      {!pagesRange.includes(1) && (
        <>
          <Button onClick={() => setPage(1)}>{1}</Button>
          <span className={styles.separator}>• • •</span>
        </>
      )}
      {pagesRange.map((page) => (
        <Button
          key={page}
          onClick={() => setPage(page)}
          className={clsx(page === currentPage && styles.current)}
        >
          {page}
        </Button>
      ))}
      {!pagesRange.includes(pagesCount) && (
        <>
          <span className={styles.separator}>• • •</span>
          <Button onClick={() => setPage(pagesCount)}>{pagesCount}</Button>
        </>
      )}
      <Button
        onClick={() => setPage(currentPage + 1)}
        disabled={currentPage === pagesCount}
        className={styles.arrow}
      >
        {'>'}
      </Button>
    </div>
  );
};

export const Pagination = reflect({
  view: PaginationView,
  bind: {
    pagesCount: paginationModel.selectors.$pagesCount,
    currentPage: paginationModel.selectors.$currentPage,
    pagesRange: paginationModel.selectors.$pagesRange,
    setPage: paginationModel.events.setPage,
  },
});

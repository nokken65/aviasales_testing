import { createEvent, restore, sample } from 'effector';

import { filterModel } from '@/features/Filter';
import { SORT_OPTIONS } from '@/shared/constants';
import { sortTickets } from '@/shared/utils';

const changeActive = createEvent<SORT_OPTIONS>();
const $active = restore<SORT_OPTIONS>(changeActive, SORT_OPTIONS.Price);

sample({
  clock: [changeActive, filterModel.selectors.$filteredTickets],
  source: { tickets: filterModel.selectors.$filteredTickets, sort: $active },
  target: filterModel.selectors.$filteredTickets,
  fn: sortTickets,
});

export const selectors = {
  $active,
};

export const events = {
  changeActive,
};

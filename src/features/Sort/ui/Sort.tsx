import { reflect } from '@effector/reflect';
import { useTranslation } from 'react-i18next';

import { SORT_OPTIONS } from '@/shared/constants';
import { Tab } from '@/shared/ui/Tab';

import { sortModel } from '..';

type SortProps = {
  options: Array<string>;
  active: SORT_OPTIONS;
  changeActive: (id: SORT_OPTIONS) => void;
};

const SortView = ({ options, active, changeActive }: SortProps) => {
  const { t } = useTranslation();
  return (
    <Tab>
      {options.map((item, id) => (
        <Tab.Item
          label={t(`sort.${item}`)}
          key={id}
          active={active === id}
          onActive={() => changeActive(id)}
        />
      ))}
    </Tab>
  );
};

export const Sort = reflect({
  view: SortView,
  bind: {
    options: ['price', 'duration', 'optimal'],
    active: sortModel.selectors.$active,
    changeActive: sortModel.events.changeActive,
  },
});

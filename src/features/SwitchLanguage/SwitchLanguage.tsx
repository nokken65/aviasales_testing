import { useTranslation } from 'react-i18next';

import { Button } from '@/shared/ui';

import styles from './SwitchLanguage.module.scss';

export const SwitchLanguage = () => {
  const { i18n } = useTranslation();
  return (
    <div className={styles.switcher}>
      <Button
        className={i18n.language === 'en' ? styles.active : ''}
        onClick={() => i18n.changeLanguage('en')}
      >
        EN 🇺🇸
      </Button>
      <Button
        className={i18n.language === 'ru' ? styles.active : ''}
        onClick={() => i18n.changeLanguage('ru')}
      >
        RU 🇷🇺
      </Button>
    </div>
  );
};

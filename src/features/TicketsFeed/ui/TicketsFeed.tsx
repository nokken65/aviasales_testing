import { variant } from '@effector/reflect';
import { combine } from 'effector';

import { ticketModel } from '@/entities/Ticket';
import { filterModel } from '@/features/Filter';
import { Pagination } from '@/features/Pagination';
import { Empty, Loader } from '@/shared/ui';

import styles from './TicketsFeed.module.scss';
import { TicketsFeedList } from './TicketsFeedList';

const TicketsFeedView = () => {
  return (
    <>
      <Pagination />
      <ul className={styles.ticketsFeed}>
        <TicketsFeedList />
      </ul>
    </>
  );
};

export const TicketsFeed = variant({
  source: combine(
    {
      isEmpty: ticketModel.selectors.$ticketsIsEmpty,
      isFilteredEmpty: filterModel.selectors.$filteredTicketsIsEmpty,
    },
    ({ isEmpty, isFilteredEmpty }) => {
      if (isEmpty) return 'empty';
      if (isFilteredEmpty) return 'filteredEmpty';
      return 'ready';
    }
  ),
  cases: {
    empty: () => <Loader.Ripple />,
    filteredEmpty: () => <Empty />,
    ready: TicketsFeedView,
  },
  hooks: {
    mounted: ticketModel.effects.getSearchIdFx.prepend(() => {}),
  },
});

import { list } from '@effector/reflect';
import { v4 as uuidv4 } from 'uuid';

import { TicketCard } from '@/entities/Ticket';
import { paginationModel } from '@/features/Pagination';

type TicketsFeedItemProps = {
  ticket: import('@/shared/api').Ticket;
};

const TicketsFeedItemView = ({ ticket }: TicketsFeedItemProps) => {
  return (
    <li>
      <TicketCard ticket={ticket} />
    </li>
  );
};

export const TicketsFeedList = list({
  view: TicketsFeedItemView,
  source: paginationModel.selectors.$currentTickets,
  mapItem: {
    ticket: (ticket) => ticket,
  },
  getKey: () => uuidv4(),
});

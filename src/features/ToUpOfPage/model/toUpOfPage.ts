import { createEvent, restore } from 'effector';
import { debounce } from 'patronum';

const onScroll = createEvent<number>();
const setScrollY = debounce({ source: onScroll, timeout: 200 });

const $scrollY = restore<number>(setScrollY, 0);

export const events = {
  onScroll,
};

export const selectors = {
  $scrollY,
};

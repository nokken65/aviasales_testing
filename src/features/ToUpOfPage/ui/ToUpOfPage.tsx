import { reflect } from '@effector/reflect';
import clsx from 'clsx';

import { Button } from '@/shared/ui';

import { toUpOfPageModel } from '..';
import styles from './ToUpOfPage.module.scss';

type ToUpOfPageProps = { scrollY: number };

const ToUpOfPageView = ({ scrollY }: ToUpOfPageProps) => {
  return (
    <Button
      className={clsx(
        styles.button,
        scrollY > window.innerHeight && styles.show
      )}
      onClick={() => window.scrollTo({ top: 0, behavior: 'smooth' })}
    >
      <svg className={styles.arrow}>
        <use href='/assets/icons/arrow.svg#arrow' />
      </svg>
    </Button>
  );
};

export const ToUpOfPage = reflect({
  view: ToUpOfPageView,
  bind: {
    scrollY: toUpOfPageModel.selectors.$scrollY,
  },
  hooks: {
    mounted: () =>
      document.addEventListener('scroll', () =>
        toUpOfPageModel.events.onScroll(window.scrollY)
      ),
    unmounted: () =>
      document.removeEventListener('scroll', () =>
        toUpOfPageModel.events.onScroll(window.scrollY)
      ),
  },
});

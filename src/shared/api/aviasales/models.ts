export type TicketSegment = {
  origin: string;
  destination: string;
  date: string;
  stops: Array<string>;
  duration: number;
};

export type Ticket = {
  price: number;
  carrier: string;
  segments: Array<TicketSegment>;
};

export type SearchId = {
  searchId: string;
};

export type TicketsBundle = {
  tickets: Array<Ticket>;
  stop: boolean;
};

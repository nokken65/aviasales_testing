import { SearchId } from '..';
import { apiInstance } from './base';

export const getSearchId = async (): Promise<SearchId> => {
  return (await apiInstance.get('search')).data;
};

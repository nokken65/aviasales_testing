import { Ticket, TicketsBundle } from '..';
import { apiInstance } from './base';

const request = async (searchId: string): Promise<TicketsBundle> => {
  try {
    return (await apiInstance.get(`tickets?searchId=${searchId}`)).data;
  } catch (error) {
    throw new Error();
  }
};

const requestAllBundles = async (searchId: string): Promise<Array<Ticket>> => {
  const promises = [...Array(5)].map(() => request(searchId));
  const data = (await Promise.allSettled(promises)).reduce(
    (data, res) => {
      switch (res.status) {
        case 'fulfilled':
          return {
            stop: res.value.stop,
            tickets: [...data.tickets, ...res.value.tickets],
          };
        case 'rejected':
          return data;
      }
    },
    { stop: false, tickets: [] } as TicketsBundle
  );

  return data.stop
    ? data.tickets
    : [...data.tickets, ...(await requestAllBundles(searchId))];
};

export const getTickets = async ({
  searchId,
}: {
  searchId: string;
}): Promise<Ticket[]> => {
  return await requestAllBundles(searchId);
};

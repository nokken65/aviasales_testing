import clsx from 'clsx';
import { HTMLAttributes } from 'react';

import styles from './Button.module.scss';

type ButtonProps = HTMLAttributes<HTMLButtonElement> & {
  disabled?: boolean;
};

export const Button = ({
  disabled,
  className,
  children,
  onClick,
}: ButtonProps) => {
  return (
    <button
      className={clsx(styles.button, className, disabled && styles.disabled)}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

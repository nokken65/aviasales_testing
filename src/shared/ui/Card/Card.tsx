import clsx from 'clsx';
import { PropsWithChildren } from 'react';

import styles from './Card.module.scss';

type CardProps = PropsWithChildren<{ className?: string }>;

export const Card = ({ children, className }: CardProps) => {
  return <div className={clsx(styles.card, className)}>{children}</div>;
};

import { PropsWithChildren } from 'react';

import styles from './Checklist.module.scss';
import { Item } from './Item';

type ChecklistProps = PropsWithChildren<{}>;

const Checklist = ({ children }: ChecklistProps) => {
  return <ul className={styles.checklist}>{children}</ul>;
};

Checklist.Item = Item;

export { Checklist };

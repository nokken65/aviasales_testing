import styles from './Item.module.scss';

type ItemProps = {
  label: string;
  active?: boolean;
  onActive?: () => void;
};

export const Item = ({ label, active, onActive }: ItemProps) => {
  return (
    <li className={styles.wrapper}>
      <input
        className={styles.box}
        type='checkbox'
        name={label}
        id={label}
        checked={active}
        onChange={onActive}
      />
      <label className={styles.label} htmlFor={label}>
        {label}
      </label>
    </li>
  );
};

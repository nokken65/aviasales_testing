import styles from './Empty.module.scss';

type EmptyProps = {
  size?: number;
};

export const Empty = ({ size = 100 }: EmptyProps) => {
  return (
    <div className={styles.empty} style={{ fontSize: `${size}px` }}>
      <span>😟</span>
      <p>empty...</p>
    </div>
  );
};

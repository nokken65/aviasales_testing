import { PropsWithChildren } from 'react';

import styles from './Heading.module.scss';

type HeadingProps = PropsWithChildren<{}>;

export const Heading = ({ children }: HeadingProps) => {
  return <p className={styles.heading}>{children}</p>;
};

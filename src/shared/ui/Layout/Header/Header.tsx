import { PropsWithChildren } from "react";
import styles from "./Header.module.scss";

type HeaderProps = PropsWithChildren<{}>;

const Header = ({ children }: HeaderProps) => {
  return <header className={styles.header}>{children}</header>;
};

export default Header;

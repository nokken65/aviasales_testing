import { PropsWithChildren } from 'react';

import { Container } from './Container';
import Header from './Header/Header';
import styles from './Layout.module.scss';
import Main from './Main/Main';

type LayoutProps = PropsWithChildren<{}>;

const Layout = ({ children }: LayoutProps) => {
  return <div className={styles.layout}>{children}</div>;
};

Layout.Header = Header;
Layout.Main = Main;
Layout.Container = Container;

export { Layout };

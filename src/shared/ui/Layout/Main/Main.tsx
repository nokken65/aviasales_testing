import { PropsWithChildren } from 'react';

import styles from './Main.module.scss';

type MainProps = PropsWithChildren<{}>;

const Main = ({ children }: MainProps) => {
  return <main className={styles.main}>{children}</main>;
};

export default Main;

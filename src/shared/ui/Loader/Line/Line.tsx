import styles from './Line.module.scss';

export const Line = () => {
  return (
    <span className={styles.line}>
      <span className={styles.lineElement} />
    </span>
  );
};

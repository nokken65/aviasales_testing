import { Line } from './Line';
import { Ripple } from './Ripple';

export const Loader = {
  Line,
  Ripple,
};

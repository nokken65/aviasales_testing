import styles from './Ripple.module.scss';

export const Ripple = () => {
  return (
    <div className={styles.wrapper}>
      <span className={styles.ripple}>
        <span />
        <span />
      </span>
    </div>
  );
};

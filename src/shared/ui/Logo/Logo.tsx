import styles from './Logo.module.scss';

type LogoProps = {
  src: string;
};

export const Logo = ({ src }: LogoProps) => {
  return (
    <img
      src={src}
      className={styles.logo}
      loading='lazy'
      width={60}
      height={65}
      alt='logo'
    />
  );
};

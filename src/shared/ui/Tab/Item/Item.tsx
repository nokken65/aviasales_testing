import clsx from 'clsx';

import styles from './Item.module.scss';

type ItemProps = {
  label: string;
  active?: boolean;
  onActive: () => void;
};

export const Item = ({ label, active, onActive }: ItemProps) => {
  return (
    <button
      className={clsx(styles.item, active && styles.active)}
      onClick={onActive}
    >
      {label}
    </button>
  );
};

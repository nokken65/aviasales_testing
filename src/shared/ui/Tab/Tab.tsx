import { PropsWithChildren } from 'react';

import { Item } from './Item';
import styles from './Tab.module.scss';

type TabProps = PropsWithChildren<{}>;

const Tab = ({ children }: TabProps) => {
  return <div className={styles.tab}>{children}</div>;
};

Tab.Item = Item;

export { Tab };

import { Ticket } from '../api';
import { TRANSFER_OPTIONS } from '../constants';

type FilterTicketsProps = {
  tickets: Array<Ticket>;
  filter: Array<TRANSFER_OPTIONS>;
};

export const filterTickets = ({
  tickets,
  filter,
}: FilterTicketsProps): Array<Ticket> => {
  return filter.reduce((data, option) => {
    switch (option) {
      case TRANSFER_OPTIONS.All:
        return tickets;
      default:
        return [
          ...data,
          ...tickets.filter(
            (ticket) =>
              ticket.segments[0].stops.length +
                ticket.segments[1].stops.length ===
              option
          ),
        ];
    }
  }, [] as Array<Ticket>);
};

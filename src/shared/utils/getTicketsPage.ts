import { Ticket } from '../api';

type GetTicketsPageProps = {
  tickets: Array<Ticket>;
  page: number;
  pageSize: number;
};

export const getTicketsPage = ({
  tickets,
  page,
  pageSize,
}: GetTicketsPageProps): Array<Ticket> => {
  const start = (page - 1) * pageSize;
  const end =
    start + pageSize > tickets.length ? tickets.length : start + pageSize;
  return tickets.slice(start, end);
};

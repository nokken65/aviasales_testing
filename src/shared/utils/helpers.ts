import i18n from '@/app/i18n';

export const formatPrice = (price: number): string => {
  return price
    .toString()
    .split('')
    .reverse()
    .map((item, index) =>
      (index + 1) % 3 === 0 ? ' ' + item : item.toString()
    )
    .reverse()
    .join('');
};

export const formatFlyDurartion = (duration: number): string => {
  const days = Math.floor(duration / 60 / 24);
  const hours = Math.floor(duration / 60);

  if (days) {
    const hoursLast = hours - days * 24;
    return `${days}${i18n.t('time.day')} ${hoursLast}${i18n.t('time.hour')}`;
  } else if (hours) {
    const minutes = duration - hours * 60;
    return `${hours}${i18n.t('time.hour')} ${minutes}${i18n.t('time.minute')}`;
  } else {
    return `${duration}${i18n.t('time.minute')}`;
  }
};

export const formatFlyTime = (time: number): string => {
  const date = new Date(time);
  return `${addZero(date.getHours())}:${addZero(date.getMinutes())}`;
};

const addZero = (time: number): string => {
  return time.toString().split('').length === 1 ? '0' + time : '' + time;
};

type PagesRangeProps = {
  page: number;
  pagesCount: number;
};

const range = (start: number, end: number) => {
  return [...Array(end - start + 1)].map((_, idx) => start + idx);
};

// TODO
// need remake
export const pagesRange = ({
  page,
  pagesCount,
}: PagesRangeProps): Array<number> => {
  const size = 3;

  const arr = range(page - size, page + size);
  const t = arr.filter((v) => v >= 1 && v <= pagesCount);
  const b = [...Array(size * 2 + 1 - t.length)].reduce(
    (acc) => {
      if (t[0] < size) {
        return [...acc, acc[acc.length - 1] + 1];
      }
      if (t[t.length - 1] > pagesCount - size) {
        return [acc[0] - 1, ...acc];
      }
      return [...acc];
    },
    [...t]
  );
  return b;
};

import { Ticket } from '../api';
import { SORT_OPTIONS } from '../constants';

type SortTicketsProps = {
  tickets: Array<Ticket>;
  sort: SORT_OPTIONS;
};

export const sortTickets = ({
  tickets,
  sort,
}: SortTicketsProps): Array<Ticket> => {
  switch (sort) {
    case SORT_OPTIONS.Price:
      return tickets.sort((a, b) => a.price - b.price);
    case SORT_OPTIONS.Duration:
      return tickets.sort(
        (a, b) =>
          a.segments[0].duration +
          a.segments[1].duration -
          (b.segments[0].duration + b.segments[1].duration)
      );
    case SORT_OPTIONS.Optimal:
      return tickets.sort(
        (a, b) =>
          a.price +
          (a.segments[0].duration + a.segments[1].duration) * 100 -
          (b.price + (b.segments[0].duration + b.segments[1].duration) * 100)
      );
    default:
      return tickets;
  }
};
